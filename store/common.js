export const state = () => ({
  shifts: [],
});

export const mutations = {
  setShifts(state, payload) {
    state.shifts.push(payload);
  },
};

export const actions = {};

export const getters = {
  getShifts(state) {
    return state.shifts;
  },
};
